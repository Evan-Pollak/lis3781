# LIS3781 - Advanced Database Management

## Evan Pollak

### Assignment 3 Requirements:
1. Engineer tables and insert statements using SQL
2. Foward engineer tables to Oracle Server via RemoteLabs
3. Answer theory questions from textbook in Chapter 12
4. Create various select statements and execute them on the database on the Oracle Server

### Assignment Screenshots:
SQL Code 1 | SQL Code 2
- | -
![alt](img/1.png) | ![alt](img/2.png)

Customer Table | Commodity Table
- | -
![alt](img/3.png) | ![alt](img/4.png)

"order" Table| *
- | -
![alt](img/5.png) | ![alt](img/10.png)
