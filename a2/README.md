
# LIS3781 - Advanced Database Management

## Evan Pollak

### Assignment 2 Requirements:
1. Create Local MySQL database under FSUID
2. Engineer tables and insert statements using SQL
3. Create additional test users
4. Grant various permissions to test uesrs
5. Test granted permissions through various MySQL queries

### Assignment Screenshots:

*Company Table Code*:
![alt](img/1.png) 

*Customer Table Code*:
![alt](img/2.png)

*Populated Tables*:
![alt](img/3.png)
