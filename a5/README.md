# LIS3781 - Assignment 5

## Evan Pollak

### Assignment Requirements
1. Engineer tables and insert statements based on business rules using MS SQL
2. Foward engineer populated tables to RemoteLabs MS SQL Server
3. Answer theory question from textbook in Chapter 15
4. Create triggers, views, and procedures to maniuplate and display data from the database

### Assignment Screenshots:

*ERD*
![alt](img/a5_erd_epollak.PNG)

*Statements*
![alt](img/a5_statements.PNG)