
# LIS3781 - Advanced Database Management

## Evan Pollak

### LIS3781 Requirements:

*Course Work Links*:

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Install AMPPS
    * Create Entity Relationship Diagram via MySQL Workbench
    * Setup Bitbucket Repo
    * Provide short descriptions of git commands

2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Create Local MySQL database under FSUID
    * Engineer tables and insert statements using SQL
    * Create additional test users
    * Grant various permissions to test uesrs
    * Test granted permissions through various MySQL queries

3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Engineer tables and insert statements using SQL
    * Foward engineer tables to Oracle Server via RemoteLabs
    * Answer theory questions from textbook in Chapter 12
    * Create various select statements and execute them on the database on the Oracle Server

4. [P1 README.md](p1/README.md "My P1 README.md file")
    * Engineer tables and insert statements using MySQL
    * Forward engineer tables to cci servers using SSH
    * Answer theory question from textbook in Chapter 13
    * Create triggers and procedures to manipulate and display data from the database

5. [A4 README.md](a4/README.md "My A4 README.md file")
    * Engineer tables and insert statements using MS SQL
    * Foward engineer populated tables to RemoteLabs MS SQL Server
    * Answer theory questions from textbook in Chapter 14
    * Create triggers, views, and procedures to maniuplate and display data from the database

6. [A5 README.md](a5/README.md "My A5 README.md file")
    * Engineer tables and insert statements based on business rules
    * Foward engineer populated tables to RemoteLabs MS SQL Server
    * Answer theory questions from textbook in Chapter 15
    * Create triggers, views, and procedures to maniuplate and display data from the database

7. [P2 README.md](p2/README.md "My P2 README.md file")
    * Create MongoDB cluster
    * Install MongoDB shell and connect to created cluster
    * Answer theory question from textbook in Chapter 16
    * Import JSON dataset to cluster
    * Create and execute queries to display data from restaurants table in test DB