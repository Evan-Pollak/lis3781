
# LIS3781 - Advanced Database Management

## Evan Pollak

### Assignment 1 Requirements:
1. Install AMPPS
2. Create ERD based on A1 business rules
3. Foward Engineer Database
4. Setup Bitbucket Repo
5. Provide short descriptions of git commands

### Git Commands:
1. git init - Create new repository or initilizate an existing one
2. git status - See what files need to be added from local or which ones have been deleted
3. git add - Add new/modified file from local repo
4. git commit - Creates snapshot of the repo with the new changes
5. git push - Upload local repo to remote one
6. git pull - Download remote repo content to local one
7. git clone - Create copy of existing repo

### Assignment Screenshots:

*Ampps*:
![alt](img/ampps.png) 

*A1 ERD*:
![alt](img/erd.png)

*A1 Ex1*:
![alt](img/q1.png)


### Assignment Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Evan-Pollak/bitbucketstationlocations/src/master/)

*Assignment 1:*
[A1 Repo Link](https://bitbucket.org/Evan-Pollak/lis3781/src/master/a1/)

