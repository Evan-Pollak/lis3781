# LIS3781 - Project 1

## Evan Pollak

### Assignment Requirements
1. Engineer tables and insert statements using MySQL
2. Foward engineer populated tables to cci server
3. Answer theory question from textbook in Chapter 13
4. Create triggers and procedures to maniuplate and display data from the database

### Assignment Screenshots:

*Business Rules*
![alt](img/2.png)

*ERD*
![alt](img/1.png)

