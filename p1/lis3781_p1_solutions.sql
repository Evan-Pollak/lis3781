--Evan Pollak--
--Project 1 Solutions--
--Due March 4, 2020--


select 'drop, create, use database, create tables, display data:' as '';
do sleep(5);

DROP SCHEMA IF EXISTS edp19c;
CREATE SCHEMA IF NOT EXISTS edp19c;

SHOW WARNINGS;
USE edp19c;

-- TABLE PERSON
DROP TABLE IF EXISTS person;
CREATE TABLE IF NOT EXISTS person
(
  per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  per_ssn BINARY(64) NULL,
  per_salt BINARY(64) NULL,
  per_fname VARCHAR(15) NOT NULL,
  per_lname VARCHAR(30) NOT NULL,
  per_street VARCHAR(30) NOT NULL,
  per_city VARCHAR(30) NOT NULL,
  per_state CHAR(2) NOT NULL,
  per_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
  per_email VARCHAR(100) NOT NULL,
  per_dob DATE NOT NULL,
  per_type ENUM('a','c','j') NOT NULL,
  per_notes VARCHAR(255)NULL,
  PRIMARY KEY(per_id),
  UNIQUE INDEX ux_per_ssn(per_ssn ASC)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;
SHOW WARNINGS;

-- TABLE ATTORNEY
DROP TABLE IF EXISTS attorney;
CREATE TABLE IF NOT EXISTS attorney
(
  per_id SMALLINT UNSIGNED NOT NULL,
  aty_start_date DATE NOT NULL,
  aty_end_date DATE NULL DEFAULT NULL,
  aty_hourly_rate DECIMAL(5,2) UNSIGNED NOT NULL,
  aty_years_in_practice TINYINT NOT NULL,
  aty_notes VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (per_id),

  INDEX idx_per_id (per_id ASC),

  CONSTRAINT fk_attorney_person
    FOREIGN KEY (per_id)
    REFERENCES person (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- TABLE CLIENT
DROP TABLE IF EXISTS client;
CREATE TABLE IF NOT EXISTS client
(
  per_id SMALLINT UNSIGNED NOT NULL,
  cli_notes VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (per_id),

  INDEX idx_per_id (per_id ASC),

  CONSTRAINT fk_client_person
    FOREIGN KEY (per_id)
    REFERENCES person (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- TABLE COURT
DROP TABLE IF EXISTS court;
CREATE TABLE IF NOT EXISTS court
(
  crt_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  crt_name VARCHAR(45) NOT NULL,
  crt_street VARCHAR(30) NOT NULL,
  crt_city VARCHAR(30) NOT NULL,
  crt_state CHAR(2) NOT NULL,
  crt_zip INT(9) UNSIGNED ZEROFILL NOT NULL,
  crt_phone BIGINT NOT NULL,
  crt_email VARCHAR(100) NOT NULL,
  crt_url VARCHAR(100) NOT NULL,
  crt_notes VARCHAR(255) NULL,
  PRIMARY KEY (crt_id)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- TABLE JUDGE
DROP TABLE IF EXISTS judge;
CREATE TABLE IF NOT EXISTS judge
(
  per_id SMALLINT UNSIGNED NOT NULL,
  crt_id TINYINT UNSIGNED NULL DEFAULT NULL,
  jud_salary DECIMAL(8,2) NOT NULL,
  jud_years_in_practice TINYINT UNSIGNED NOT NULL,
  jud_notes VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (per_id),

  INDEX idx_per_id (per_id ASC),
  INDEX idx_crt_id (crt_id ASC),

  CONSTRAINT fk_judge_person
    FOREIGN KEY (per_id)
    REFERENCES person (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

  CONSTRAINT fk_judge_court
    FOREIGN KEY (crt_id)
    REFERENCES court (crt_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- TABLE JUDGE_HISTORY
DROP TABLE IF EXISTS judge_hist;
CREATE TABLE IF NOT EXISTS judge_hist
(
  jhs_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  per_id SMALLINT UNSIGNED NOT NULL,
  jhs_crt_id TINYINT NULL,
  jhs_date timestamp NOT NULL DEFAULT current_timestamp(),
  jhs_type ENUM('i','u','d') NOT NULL default 'i',
  jhs_salary DECIMAL(8,2) NOT NULL,
  jhs_notes VARCHAR(255) NULL,
  PRIMARY KEY (jhs_id),

  INDEX idx_per_id (per_id ASC),

  CONSTRAINT fk_judge_hist_judge
    FOREIGN KEY (per_id)
    REFERENCES judge (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- TABLE `case`
DROP TABLE IF EXISTS `case`;
CREATE TABLE IF NOT EXISTS `case`
(
  cse_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  per_id SMALLINT UNSIGNED NOT NULL,
  cse_type VARCHAR(45) NOT NULL,
  cse_description TEXT NOT NULL,
  cse_start_date DATE NOT NULL,
  cse_end_date DATE NULL,
  cse_notes VARCHAR(255) NULL,
  PRIMARY KEY (cse_id),

  INDEX idx_per_id (per_id ASC),

  CONSTRAINT fk_court_case_judge
    FOREIGN KEY (per_id)
    REFERENCES judge (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

--TABLE bar
DROP TABLE IF EXISTS bar;
CREATE TABLE IF NOT EXISTS bar
(
  bar_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  per_id SMALLINT UNSIGNED NOT NULL,
  bar_name VARCHAR(45) NOT NULL,
  bar_notes VARCHAR(255) NULL,
  PRIMARY KEY (bar_id),

  CONSTRAINT fk_bar_attoryney
    FOREIGN KEY (per_id)
    REFERENCES attorney (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- TABLE specialty
DROP TABLE IF EXISTS specialty;
CREATE TABLE IF NOT EXISTS specialty
(
  spc_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  per_id SMALLINT UNSIGNED NOT NULL,
  spc_type VARCHAR(45) NOT NULL,
  spc_notes VARCHAR(255) NULL,
  PRIMARY KEY (spc_id),

  INDEX idx_per_id (per_id ASC),
  CONSTRAINT fk_specialty_attorney
    FOREIGN KEY (per_id)
    REFERENCES attorney (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- TABLE assignment
DROP TABLE IF EXISTS assignment;
CREATE TABLE IF NOT EXISTS assignment
(
  asn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  per_cid SMALLINT UNSIGNED NOT NULL,
  per_aid SMALLINT UNSIGNED NOT NULL,
  cse_id SMALLINT UNSIGNED NOT NULL,
  asn_notes VARCHAR(255) NULL,
  PRIMARY KEY (asn_id),

  INDEX idx_per_cid (per_cid ASC),
  INDEX idx_per_aid (per_aid ASC),
  INDEX idx_cse_id (cse_id ASC),

  UNIQUE INDEX ux_per_cid_per_aid_cse_id (per_cid ASC, per_aid ASC, cse_id ASC),

  CONSTRAINT fk_assign_case
    FOREIGN KEY (cse_id)
    REFERENCES `case` (cse_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

  CONSTRAINT fk_assignment_client
    FOREIGN KEY (per_cid)
    REFERENCES client (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

  CONSTRAINT fk_assignment_attorney
    FOREIGN KEY (per_aid)
    REFERENCES attorney (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- TABLE PHONE
DROP TABLE IF EXISTS phone;
CREATE TABLE IF NOT EXISTS phone
(
  phn_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  per_id SMALLINT UNSIGNED NOT NULL,
  phn_num BIGINT UNSIGNED NOT NULL,
  phn_type ENUM('h','c','w','f') NOT NULL COMMENT 'home, cell, work, fax',
  phn_notes VARCHAR(255) NULL,
  PRIMARY KEY (phn_id),

  INDEX idx_per_id (per_id ASC),

  CONSTRAINT fk_phone_person
    FOREIGN KEY (per_id)
    REFERENCES person (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET=utf8
COLLATE = utf8_unicode_ci;

SHOW WARNINGS;

-- DATA TABLE PERSON
START TRANSACTION;

INSERT INTO person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, NULL, 'Steve','Rogers','437 Southern Drive','Rochester','NY',324402222,'srogers@comcast.net','1923-10-03','c',NULL),
(NULL, NULL, NULL, 'Bruce','Wayne','1007 Mountain Drive','Gotham','NY',003208440,'bwayne@knology.net','1968-03-20','c',NULL),
(NULL, NULL, NULL, 'Peter','Parker','20 Ingram Street','New York','NY',102872341,'pparker@msn.com','1988-09-12','c',NULL),
(NULL, NULL, NULL, 'Jane','Thompson','13563 Ocean View Drive','Seattle','WA',032084409,'jthompson@gmail.com','1978-05-08','c',NULL),
(NULL, NULL, NULL, 'Debra','Steele','543 Oak Ln','Milwaukee','WI',286234178,'dsteele@verizon.net','1994-07-19','c',NULL),
(NULL, NULL, NULL, 'Tony','Stark','332 Palm Avenue','Malibu','CA',902638332,'tstark@yahoo.com','1972-05-04','a',NULL),
(NULL, NULL, NULL, 'Hank','Pymi','2355 Brown Street','Cleveland','OH',022348890,'hpym@aol.com','1980-08-28','a',NULL),
(NULL, NULL, NULL, 'Bob','Best','4902 Avendale Avanue','Scottsdale','AZ',872638332,'bbest@yahoo.com','1992-02-10','a',NULL),
(NULL, NULL, NULL, 'Sandra','Dole','87912 Lawrence Ave','Atlanta','GA',002348890,'sdole@gmail.com','1990-01-26','a',NULL),
(NULL, NULL, NULL, 'Ben','Avery','6432 Thunderbird Ln','Sioux Falls','SD',562638332,'bavery@hotmail.com','1983-12-24','a',NULL),
(NULL, NULL, NULL, 'Arthur','Curry','3304 Euclid Avenue','Miami','FL',000219932,'acurry@gmail.com','1975-12-15','j',NULL),
(NULL, NULL, NULL, 'Diana','Price','944 Green Street','Las Vegas','NV',332048823,'dprice@symaptico.com','1980-08-22','j',NULL),
(NULL, NULL, NULL, 'Adam','Jurris','98435 Valencia Dr.','Gulf Shores','AL',870219932,'ajurris@gmx.com','1995-01-31','j',NULL),
(NULL, NULL, NULL, 'Judy','Sleen','56343 Rover Ct.','Billings','MT',672048823,'jsleen@symaptico.com','1970-03-22','j',NULL),
(NULL, NULL, NULL, 'Bill','Neiderheim','43567 Netherland Blvd','South Bend','IN',320219932,'bneiderheim@comast.net','1982-03-13','j',NULL);

COMMIT;

-- DATA TABLE phone
START TRANSACTION;

INSERT INTO phone
(phn_id, per_id, phn_num, phn_type, phn_notes)
VALUES
  (NULL, 1, 8032288827,'c',NULL),
  (NULL, 2, 2052338293,'h',NULL),
  (NULL, 4, 1034325598,'w','has two office numbers'),
  (NULL, 5, 6402338494,'w',NULL),
  (NULL, 6, 5508329842,'f','fax numbers not currently working'),
  (NULL, 7, 8202052203,'c','prefers home calls'),
  (NULL, 8, 4008338294,'h',NULL),
  (NULL, 9, 7654328912,'w',NULL),
  (NULL, 10, 5463721984,'f','work fax number'),
  (NULL, 11, 4537821902,'h','prefers cell phone calls'),
  (NULL, 12, 7867821902,'w','best number to reach'),
  (NULL, 13, 4537821654,'w','call during lunch'),
  (NULL, 14, 3721821902,'c','prefers cell phone calls'),
  (NULL, 15, 9217821945,'f','use for faxing legal docs');

COMMIT;

-- DATA TABLE CLIENT
START TRANSACTION;

INSERT INTO client
(per_id, cli_notes)
VALUES
(1, NULL),
(2, NULL),
(3, NULL),
(4, NULL),
(5, NULL);

COMMIT;

-- DATA TABLE ATTORNEY
START TRANSACTION;

INSERT INTO attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
VALUES
(6, '2006-06-12', NULL, 85, 5, NULL),
(7, '2003-08-20', NULL, 130, 28, NULL),
(8, '2009-12-12', NULL, 70, 17, NULL),
(9, '2008-06-08', NULL, 78, 13, NULL),
(10, '2011-09-12', NULL, 60, 24, NULL);

COMMIT;

-- DATA TABLE BAR
START TRANSACTION;

INSERT INTO bar
(bar_id, per_id, bar_name, bar_notes)
VALUES
(NULL, 6, 'Florida bar', NULL),
(NULL, 7, 'Alabama bar', NULL),
(NULL, 8, 'Georgia bar', NULL),
(NULL, 9, 'Michigan bar', NULL),
(NULL, 10, 'South Carolina bar', NULL);

COMMIT;

-- DATA TABLE SPECIALTY
START TRANSACTION;

INSERT INTO specialty
(spc_id, per_id, spc_type, spc_notes)
VALUES
(NULL, 6, 'business', NULL),
(NULL, 7, 'traffic', NULL),
(NULL, 8, 'bankruptcy', NULL),
(NULL, 9, 'insurance', NULL),
(NULL, 10, 'juidical', NULL);

COMMIT;

-- DATA TABLE COURT
START TRANSACTION;

INSERT INTO court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
VALUES
(NULL, 'leon county circuit court','301 south monroe street','tallahassee','fl',323035292,8506065504,'lccc@us.fl.gov','http://www.leoncountycircuitcourt.gov/',NULL),
(NULL, 'leon county traffic court','1921 thomasville road','tallahassee','fl',323035292,8505774100,'lctc@us.fl.gov','http://www.leoncountytrafficcourt.gov/',NULL),
(NULL, 'florida supreme court','500 south duval street','tallahassee','fl',323035292,8504880125,'fsc@us.fl.gov','http://www.floridasupremecourt.org/',NULL),
(NULL, 'orange county courthouse','424 north orange avenue','orlando','fl',328012248,4078362000,'occ@us.fl.gov','http://www.ninthcircuit.org/',NULL),
(NULL, 'fifth district court of appeal','300 south beach street','daytona beach','fl',321158763,3862258600,'5dca@us.fl.gov','http://www.5dca.org/',NULL);

COMMIT;

-- DATA TABLE JUDGE
START TRANSACTION;

INSERT INTO judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
VALUES
(11, 5, 150000, 10, NULL),
(12, 4, 185000, 3, NULL),
(13, 4, 135000, 2, NULL),
(14, 3, 170000, 6, NULL),
(15, 1, 120000, 1, NULL);

COMMIT;

-- DATA TABLE JUDGE_HIST
START TRANSACTION;

INSERT INTO judge_hist
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
VALUES
(NULL, 11, 3, '2009-01-16','i',130000, NULL),
(NULL, 12, 2, '2010-05-27','i',140000, NULL),
(NULL, 13, 5, '2000-01-02','i',115000, NULL),
(NULL, 13, 4, '2005-07-05','i',135000, NULL),
(NULL, 14, 4, '2008-12-09','i',155000, NULL);

COMMIT;

-- DATA TABLE `case`
START TRANSACTION;

INSERT INTO `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
VALUES
(NULL, 13, 'civil','client says that his logo is being used without his consent to promote a rival business','2010-09-09',NULL,'copyright infringement'),
(NULL, 12, 'criminal','client is charged with assaulting her husband during an argument','2009-11-18','2010-12-23','assault'),
(NULL, 14, 'civil','client broke an ankle while shopping at a local grocery store. no wet floor sign was posted although the floor had just been mopped','2008-05-06','2008-07-23','slip and fall'),
(NULL, 11, 'criminal','client was charged with stealing several televisions from his former place of employment. client has a solid alibi','2011-05-20',NULL,'grand theft'),
(NULL, 13, 'criminal','client was charged with posession of 10 grams of cocaine, allegedly found in his glove box by state police','2011-06-05',NULL,'possession of narcotics');

COMMIT;

-- DATA TABLE ASSIGNMENT
START TRANSACTION;

INSERT INTO assignment
(asn_id, per_cid, per_aid, cse_id, asn_notes)
VALUES
(NULL, 1, 6, 1, NULL),
(NULL, 2, 6, 3, NULL),
(NULL, 3, 7, 2, NULL),
(NULL, 4, 8, 2, NULL),
(NULL, 5, 9, 5, NULL);

COMMIT;

-- PERSON HASH
DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN
  DECLARE x, y INT;
  SET x = 1;

  select count(*) into y from person;

    while x <= y DO
      SET @salt=RANDOM_BYTES(64);
      SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111;
      SET @ssn=unhex(sha2(concat(@salt, @ran_num),512));

    update person
    set per_ssn=@ssn, per_salt=@salt
    where per_id=x;

  SET x = x + 1;

  END WHILE;

END$$
DELIMITER ;
call CreatePersonSSN();



--1--

drop VIEW if exists v_attorney_info;
CREATE VIEW v_attorney_info AS
  select
  concat(per_lname, ", ", per_fname) as name,
  concat(per_street, ", ", per_city, ", ", per_state, " ", perzip) as address,
  TIMESTAMPDIFF(year, per_dob, now()) as age,
  CONCAT('$', FORMAT(aty_hourly_rate, 2)) as hourly_rate,
  bar_name, spc_type
  from person
    natural join attorney
    natural join bar
    natural join specialty
    order by per_lname;

--2--

drop procedure if exists sp_num_judges_born_by_month;
DELIMITER //
CREATE PROCEDURE sp_num_judges_born_by_month()
BEGIN
  select month(per_dob) as month, monthname(per_dob) as month_name, count(*) as count
  from person
  natural join judge
  group by month, month_name
  order by month;
END //
DELIMITER ;

--3--

drop procedure if exists sp_cases_and_judges;
DELIMITER //
CREATE PROCEDURE sp_cases_and_judges()
BEGIN
  select per_id, case_id, cse_type, cse_description,
    concat(per_fname, " ", per_lname) as name,
    concat('(',substring(phn_num, 1, 3), ')', substring(phn_num, 4 , 3), '-', substring(phn_num, 7, 4)) as judge_phone_num,
    phn_type
    jud_years_in_practice
    cse_start_date,
    cse_end_date
  from person
    natural join judge
    natural join 'case'
    natural join phone
  where per_type='j'
  order by per_lname;
END //
DELIMITER ;

--4--

DROP TRIGGER IF EXISTS trg_judge_history_after_insert;
DELIMITER //
CREATE TRIGGER trg_judge_history_after_insert
AFTER INSERT ON JUDGE
FOR EACH ROW
BEGIN 
  INSERT INTO judge_hist
  (per_id,jhs_crt_id,jhs_date,jhs_type,jhs_salary,jhs_notes)
  VALUES
  (
  NEW.per_id, NEW.crt_id, current_timestamp(), 'i', NEW.jud_salary,
  concat("modifying user: ", user(), " Notes:", NEW.jud_notes)
  );
END //
DELIMITER ;

--5--

DROP TRIGGER IF EXISTS trg_judge_history_after_update;
DELIMITER //
CREATE TRIGGER trg_judge_history_after_update
AFTER UPDATE ON judge
FOR EACH ROW
BEGIN
  INSERT INTO judge_hist
  (per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
  VALUES
  (
  NEW.per_id, NEW.crt_id, current_timestamp(), 'u', NEW.jud_salary,
  concat("modifying user: ", user(), " Notes: ", NEW.jud_notes)
  );
END //
DELIMITER ;

--6--

