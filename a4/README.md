# LIS3781 - Assignment 4

## Evan Pollak

### Assignment Requirements
1. Engineer tables and insert statements using MS SQL
2. Foward engineer populated tables to RemoteLabs MS SQL Server
3. Answer theory question from textbook in Chapter 14
4. Create triggers, views, and procedures to maniuplate and display data from the database

### Assignment Screenshots:

*ERD*
![alt](img/a4_erd_epollak.png)

*Statements*
![alt](img/a4_statements.png)
