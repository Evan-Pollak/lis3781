# LIS3781 - Project 2 Requirements

## Evan Pollak

### Assignment Requirements
1. Create MongoDB cluster
2. Install MongoDB shell on computer
3. Answer theory question from textbook in Chapter 16
4. Import JSON dataset to cluster
5. Create and execute queries to display data from restaurants table in test DB

### Assignment Screenshots:

MongoDB shell commands | 
- | -
![alt](img/1.PNG) |